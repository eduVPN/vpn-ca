# Changes

## 5.0.1 (2024-11-21)
- update `go.mod` to set `go 1.22`
- add `lint` target to `Makefile`
- fix some issues found by `golangci-lint`

## 5.0.0 (2024-10-17)
- introduce `-alt-dns-name` flag to allow specifying "Subject Alternative Name"
  `DNSName` field in server certificates
- remove RSA support
- certificate extension improvements 
  ([#2](https://codeberg.org/eduVPN/vpn-ca/pulls/2))

## 4.0.3 (2024-08-05)
- switch from deprecated `ioutil.ReadFile` to `os.ReadFile`
- set `go 1.21` in `go.mod`
- change module name to `codeberg.org/eduVPN/vpn-ca`

## 4.0.2 (2023-09-07)
- small updates to the manual page `vpn-ca(1)`
- small code update fixing staticcheck warning

## 4.0.1 (2022-02-03)
- fix `vpn-ca(1)` manpage
- allow `umask(1)` to influence the group permissions of written key/cert
- improve `Makefile`

## 4.0.0 (2021-10-15)
- change default from RSA to ECDSA
- implement `-out-crt` and `-out-key` to write the generated certificate/key
  to a specified file
- implement `-domain-constraint` to specify DNS domain constraint for CA
- introduce `go.mod`, move Go files around
- fix bug when creating new CA with `-not-after` more than 5 years in the 
  future not working
- add vpn-ca(1) manpage

## 3.4.1 (2021-10-11)
- code cleanups
- prevent both `-client` and `-server` to be specified at the same time
- remove our own error messages where possible and simply show the value of 
  `err`

## 3.4.0 (2021-08-16)
- switch to P-384 from P-256 for ECDSA

## 3.3.0 (2021-05-26)
- support the `-ou` flag to allow specifying the "Organizational Unit" for 
  server and client certificates
  
## 3.2.0 (2021-04-21)
- support `-not-after` option when initializing the CA

## 3.1.0 (2020-09-08)
- Support ECDSA (P-256/prime256v1) and EdDSA (Ed25519) keys by setting to 
  `CA_KEY_TYPE` environment variable to either `ECDSA` or `EdDSA`

## 3.0.0 (2020-08-31)
- add name to sAN (DNSNames) for server certificates as well
- make CN of CA configurable through `-name` option when generating a CA
- change CLI parameters, now an explicit `-name` is required for `-server` and
  `-client`, optional for `-init-ca`
- default CN of CA changed to "Root CA"
- the `-init` option is renamed to `-init-ca`
- remove `-ca-dir` option, use `CA_DIR` environment variable now

## 2.0.1 (2020-04-30)
- update `Makefile` to support `install`

## 2.0.0 (2020-03-16)
- make sure issued certificates never outlive the CA (#6)
- allow specifying `CA` as a value for `-not-after` to explicitly let the 
  certificate expire at the same time as the CA (#7)
- server and client certificates now expire after 1 year by default (instead of 
  together with the CA and 90 days respectively)

## 1.0.0 (2019-11-18)
- initial release
