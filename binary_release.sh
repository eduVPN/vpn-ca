#!/bin/sh

TAG=$(git describe --abbrev=0 --tags)
OUTDIR=output/vpn-ca-${TAG}
mkdir -p ${OUTDIR}

GOOS=darwin GOARCH=amd64 go build -o vpn-ca codeberg.org/eduVPN/vpn-ca/cmd/vpn-ca/...
mkdir -p ${OUTDIR}/mac
cp vpn-ca ${OUTDIR}/mac

GOOS=windows GOARCH=amd64 go build -o vpn-ca codeberg.org/eduVPN/vpn-ca/cmd/vpn-ca/...
mkdir -p ${OUTDIR}/windows
cp vpn-ca ${OUTDIR}/windows/vpn-ca.exe

GOOS=linux GOARCH=amd64 go build -o vpn-ca codeberg.org/eduVPN/vpn-ca/cmd/vpn-ca/...
mkdir -p ${OUTDIR}/linux
cp vpn-ca ${OUTDIR}/linux

(
    cd output
    zip -r ../vpn-ca-${TAG}.zip vpn-ca-${TAG}
)

minisign -Sm vpn-ca-${TAG}.zip
